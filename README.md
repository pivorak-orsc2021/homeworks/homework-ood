# Homework #8: OOD & Rails

Let's imagine you're junior developer on first day of the job. You have already working project and you receive task to extract logic from models and controllers. You mentor helped you and prepared some high-level specs to demonstrate, what he is expecting to see as a result. 

## Task description:

1. Implement service objects, policy object & resolve all corresponding TODO's (you can comment non-related tests).
2. Before submiting merge request, make sure that all commands give successful feedback:

   ```ruby
     bundle exec rspec spec
     bundle exec rubocop
   ```

------
### Extra point(+2)

1. Implement query object & resolve all corresponding TODO's.

2. Additional specs added to cover missed cases.

### Task acceptance criteria:
* `0` - Implemented functionality that passes specs and linter check.
* `1` - There are no unresolved comments in your merge request.
* `2` - Implement additional specs for your new ruby classes (service objects, query and policy object) & configure codecoverage.
