Rails.application.routes.draw do
  post 'user_token' => 'user_token#create'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
 resources :quests, only: %i[index create show update destroy]
 resources :current_locations, only: %i[create update]
end
