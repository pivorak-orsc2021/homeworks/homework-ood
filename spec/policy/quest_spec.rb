# frozen_string_literal: true

require 'rails_helper'

RSpec.describe QuestPolicy, tdd: true do
  let(:quest) { create(:quest) }
  subject { described_class.new(current_user: user, resource: quest) }

  context 'when current user regular user' do
    let(:user) { create(:user) }

    it 'should be not able to moderate' do
      expect(subject).to respond_to(:able_to_moderate?)
      expect(subject).not_to be_able_to_moderate
    end
  end

  context 'when current user is an admin' do
    let(:user) { create(:user, role: :admin) }

    it 'should be able to moderate' do
      expect(subject).to respond_to(:able_to_moderate?)
      expect(subject).to be_able_to_moderate
    end

  end

  context 'when current user is a quest moderator' do
    let(:user) { create(:user).tap { |user| user.quests << quest } }
    it 'should be able to moderate' do
      expect(subject).to respond_to(:able_to_moderate?)
      expect(subject).to be_able_to_moderate
    end
  end
end
