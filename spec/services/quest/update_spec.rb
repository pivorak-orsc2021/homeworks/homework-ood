require 'rails_helper'

RSpec.describe Quest::Update, tdd: true do
  let(:quest) { create(:quest) }
  let!(:updated_params) { { name: "Not so good quest", description: "just bad", user_id: quest.user_id } }

  it 'update quest' do
    updated_quest = described_class.call(updated_params.merge(quest: quest))

    expect(updated_quest).to be_an_instance_of(Quest)
  end
end
