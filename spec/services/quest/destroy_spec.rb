require 'rails_helper'

RSpec.describe Quest::Destroy, tdd: true do
  let!(:quest) { create(:quest) }

  it 'delete quest' do
    destroyed_quest = described_class.call(quest)

    expect(destroyed_quest).to be_an_instance_of(Quest)
    expect(destroyed_quest).to_not be_persisted
  end
end
