# frozen_string_literal: true

require 'rails_helper'

RSpec.describe QuestQuery, tdd: true do
  let(:user) { create(:user) }
  let(:another_user) { create(:user) }

  before do
    create(:quest, user_id: user.id, is_public: true )
    create(:quest, user_id: another_user.id, is_public: true )
  end
  subject(:query) { |condition| described_class.new(user) }

  describe '#public_quests' do
    it 'returns filtered quests' do
      quests = query.public_quests

      expect(quests.size).to eq(1)
      expect(quests.first.user_id).to eq(user.id)
    end
  end

  describe '#visible_quests' do
    it 'returns all visible quests' do
      expect(query.visible_quests.size).to eq(2)
    end
  end
end
