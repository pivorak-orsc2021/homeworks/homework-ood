require 'rails_helper'

RSpec.describe Quest, type: :model, tdd: true do
  let!(:quest) { create(:quest) }
  let!(:attributes) { %i[is_public name description] }

  it 'has attributes' do
    attributes.each do |attribute|
      expect(quest).to respond_to(attribute)
    end
  end

  it 'has a valid factory' do
    expect(quest).to be_valid
  end

  context 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:description) }
  end

  context 'relations' do
    it { is_expected.to belong_to(:user) }
  end
end
