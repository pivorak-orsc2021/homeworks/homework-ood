FactoryBot.define do
  factory :quest do
    name { Faker::TvShows::SiliconValley.invention }
    description { Faker::Movies::Lebowski.quote }
    user { FactoryBot.create(:user) }
    trait :public do
      is_public { true }
    end
  end
end
