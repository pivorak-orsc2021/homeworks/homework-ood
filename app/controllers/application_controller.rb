class ApplicationController < ActionController::API
  rescue_from ActiveRecord::RecordNotFound, with: :not_found

  include Knock::Authenticable

  private

  def not_found
    render json: 'Not Found', status: :not_found
  end
end
