class CurrentLocationsController < ApplicationController
  before_action :authenticate_user, only: :update
  def create
    current_location = CurrentLocation.new(location_params)

    if current_location.save
      render(json: 'CurrentLocation was successfully created'.to_json, status: :created)
    else
      render(json: current_location.errors.full_messages, status: :unprocessable_entity)
    end
  end

  def update
    current_location = CurrentLocation.find_by(user_id: current_user.id)

    if current_location.update(location_params)
      render status: :ok, json: 'Your current location was updated'.to_json
    else
      render status: :unprocessable_entity, json: current_location.errors.full_messages
    end
  end

  private

  def location_params
    params.require(:current_location).permit(:lat, :lon, :user_id)
  end

  def current_location
    @current_location = CurrentLocation.find(params[:id])
  end
end
